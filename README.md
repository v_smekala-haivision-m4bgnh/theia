# Theia

> Theia is the Titaness of sight and by extension the goddess who endowed gold, silver and gems with their brilliance and intrinsic value.

Theia is meant to provide service to probe workflows, for example the thumber nymphs providing thumbnails.

It is a distributed service living in Juno clusters. As the expected load of websocket and HTTP connections is big, the service is meant to be light and horizontally scaled as needed, avoiding states as much as possible by leveraging on Redis and Kafka.

## Thumbnails

A number of endpoints are available to start, stop and get the thumbnail of a thumber probe from a specific hublet id. Status of running thumbers can be obtained at the topology level.

Also, an endpoint is provided for thumber nymphs to post thumbnails from remote clusters, as Theia registers a forwarder on every Vulcan cluster.

Also, a websocket endpoint is provided to receive thumbnails in binary format. As this endpoint is registered at topology level, the binary blob provided uses the following format:

- First 16 bytes: hublet UUID
- Rest of bytes: thumbnail in JPEG format

This allows to identify what hublet generated the thumbnail while avoiding bloated formats like JSON or base64.
