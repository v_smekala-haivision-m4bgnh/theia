# This library Dockerfile builds an NodeJS+TypeScript-based service with common libraries
# Your module must comply with the following:
# * have a package.json and package-lock.json
# * have a test npm script entry in package.json
# * place all your tests under `unit-tests` directory
# * have a build npm script entry in package.json
# * have a tslint.json and tsconfig.json configuration files
# * output the build result to the /app/build folder containing an App.js main code file.

#ARG TAG=latest
#ARG REPOSITORY=local

# Start off by bringing in Olympia base node common build and runner:
FROM olympia.azurecr.io/node-rdkafka-ubuntu-fips:gallium as baseBuilder
FROM olympia.azurecr.io/node-ubuntu-fips:gallium as baseRunner

############## Compiler stage:

# Reuse the base image which has dependencies installed already
FROM baseBuilder as builder

# Install Nodejs dependencies:
ADD package.json .
COPY .npmrc /app/.npmrc
COPY *.npmrc /app/
RUN cat manual.npmrc >> .npmrc || true
ADD yarn.lock .
RUN yarn install

# Configure TypeScript:
ADD jest.config.js .
ADD tslint.json .
ADD tsconfig.json .
ADD tsconfig.build.json .

# Add test code and the bulk of source code:
ADD src/ src/

# Test and build:
RUN yarn test
RUN yarn audit --level high || if [ "$?" -ge "8" ]; then exit 1; fi

RUN yarn build

# We could avoid this step and take in the runner the modules from the builder. It will be faster but the image will be heavier as it would
# contain all prod and dev packages.
FROM baseBuilder as runnerBuilder
ADD package.json .
COPY .npmrc /app/.npmrc
COPY *.npmrc /app/
RUN cat manual.npmrc >> .npmrc || true
ADD yarn.lock .
RUN yarn install --production

############## Running container build stage:
# Reuse the base image which has dependencies installed already
FROM baseRunner as runner
# Create a directory as a mount point for credential files:
CMD ["node", "/app/App.js"]
ENV NODE_ENV=prod
## Cannot yarn install again because rdfkafka is rebuilt. This will require having python installed, in that case
##  the size of the image will increase losing any beneift of installing again using --production
## Instead of installing, we take node_modules from the runnerBuilder
COPY --from=runnerBuilder /app/node_modules /app/node_modules

# Bring compiled code from the builder container
COPY --from=builder /app/build /app
COPY --from=runnerBuilder /app/package.json /
