import uuid = require("uuid");
import TYPES from "./Types";
import { Container } from "inversify";
import {
  IDBClient,
  MongoDBClient,
  IKafkaClient,
  KafkaClient,
  RedisClient,
  ICacheClient,
  IHealthRoute,
  HealthRoute,
} from "@hai/lf-node-common";
import { IHttpServer, HttpServer } from "./http/HttpServer";
import { AuthService, IAuthService } from "./auth/AuthService";
import { ThumbService, IThumbService } from "./thumbs/ThumbService";
import { IWebSocketServer, WebSocketServer } from "./http/WebSocketServer";
import { IThumbRoute, ThumbRoute } from "./thumbs/ThumbRoute";
import { ThumbWSRoute, IThumbWSRoute } from "./thumbs/ThumbWSRoute";
import { IProbeStatusRepo, ProbeStatusRepo } from "./probes/ProbeStatusRepo";

const container = new Container();
container.bind<IKafkaClient>(TYPES.IKafkaClient).to(KafkaClient).inSingletonScope();
container.bind<ICacheClient>(TYPES.ICacheClient).to(RedisClient).inSingletonScope();
container.bind<IDBClient>(TYPES.IDBClient).to(MongoDBClient).inSingletonScope();
container.bind<IHealthRoute>(TYPES.IHealthRoute).to(HealthRoute).inSingletonScope();
container.bind<IHttpServer>(TYPES.IHttpServer).to(HttpServer).inSingletonScope();
container.bind<IAuthService>(TYPES.IAuthService).to(AuthService).inSingletonScope();
container.bind<string>(TYPES.InstanceId).toConstantValue(uuid.v4());
container.bind<IThumbService>(TYPES.IThumbService).to(ThumbService).inSingletonScope();
container.bind<IProbeStatusRepo>(TYPES.IProbeStatusRepo).to(ProbeStatusRepo).inSingletonScope();
container.bind<IWebSocketServer>(TYPES.IWebSocketServer).to(WebSocketServer).inSingletonScope();

// Routes
container.bind<IThumbRoute>(TYPES.IRoute).to(ThumbRoute).inSingletonScope();
container.bind<IThumbWSRoute>(TYPES.IWSRoute).to(ThumbWSRoute).inSingletonScope();

export default container;
