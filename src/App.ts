import "dotenv/config";

import TYPES from "./Types";
import container from "./Inversify.config";
import { Config } from "./Config";
import { IHttpServer } from "./http/HttpServer";
import { createLogger, ICacheClient, IDBClient, IKafkaClient, Utils } from "@hai/lf-node-common";

export class App {
  private logger = createLogger("App");
  constructor(
    private httpServer: IHttpServer,
    private dbClient: IDBClient,
    private cacheClient: ICacheClient,
    private kafkaClient: IKafkaClient,
    private instanceId: string,
  ) {
    process.on("SIGINT", () => this.end("SIGINT"));
    process.on("SIGTERM", () => this.end("SIGTERM"));
  }

  public async main(): Promise<void> {
    try {
      this.logger.info(`Starting up ${Config.APP_NAME}!`);
      await this.dbClient.init();
      this.logger.info(`Started up ${Config.APP_NAME} db!`);
      await this.initKafka();
      this.logger.info(`Started up ${Config.APP_NAME} kafka client!`);
      await this.cacheClient.init();
      this.logger.info(`Started up ${Config.APP_NAME} cache client!`);
      const app = await this.httpServer.init();
      this.logger.info(`Started up ${Config.APP_NAME} http server!`);
    } catch (err) {
      this.logger.error("Init failed", err);
      process.exit(1);
    }
  }

  private async initKafka(): Promise<void> {
    // Init broker client
    await this.kafkaClient.init(Config.KAFKA_CONN);

    const topicInstances = Config.TOPICS_PARTITIONS;
    const kafkaInstances = Config.KAFKA_INSTANCES;

    // Create topics if needed
    for (const topic of Config.KAFKA_TOPICS_CREATE) {
      const partitions = topic.instanceConsumerGroup ? 1 : topicInstances;
      const replicationFactor = Utils.replicationLadder(kafkaInstances);
      await this.kafkaClient.createTopic(topic.name, partitions, replicationFactor);
    }

    // Init topics with theia consumer group
    this.kafkaClient.initConsumerGroup(Config.APP_NAME, [Config.THEIA_TASKS], {
      additionalConfig: { "auto.offset.reset": "latest" },
    });

    // Init the consumer group for broadcast notifications.  Notes:
    // - we use a instanceId as group id, so that each theia instance ends up in a
    //   consumer group of one and receives all messages
    // - we explicitly set the 'auto.offset.reset' to latest, which affects the
    //   starting offset assigned to new consumer groups. Due to the use of
    //   uuid(), theia is always in a new consumer group, thus it always
    //   starts at the end of the message stream.)
    this.kafkaClient.initConsumerGroup(`${Config.APP_NAME}-${this.instanceId}`, [Config.THEIA_BROADCAST], {
      additionalConfig: { "auto.offset.reset": "latest" },
    });
  }

  public async end(signal?: string): Promise<void> {
    if (signal) {
      this.logger.warn(`${signal} received`);
    }
    await Promise.all([this.kafkaClient.stop(), this.httpServer.stop(), this.cacheClient.stop(), this.dbClient.stop()]);
    process.exit(0);
  }
}

const httpServerInj = container.get<IHttpServer>(TYPES.IHttpServer);
const dbClientInj = container.get<IDBClient>(TYPES.IDBClient);
const cacheClientInj = container.get<ICacheClient>(TYPES.ICacheClient);
const kafkaClientInj = container.get<IKafkaClient>(TYPES.IKafkaClient);
const instanceIdInj = container.get<string>(TYPES.InstanceId);

new App(httpServerInj, dbClientInj, cacheClientInj, kafkaClientInj, instanceIdInj).main();
