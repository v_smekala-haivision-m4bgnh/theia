export function uniq<T>(listWithRepetitions: T[]): T[] {
  return [...new Set(listWithRepetitions)];
}

export function flatten<T>(listofLists: T[][]): T[] {
  return ([] as T[]).concat(...listofLists);
}

export function PromiseAllFlatten<T>(...listofLists: Promise<T>[][]): Promise<T[]> {
  return Promise.all(flatten(listofLists));
}

export function flatMap<T, S>(list: S[], f: (s: S) => T[]): T[] {
  return flatten<T>(list.map(f));
}

export interface IResult<T> {
  result: T | null;
  success: boolean;
  reason: any;
}

export function PromiseAllSettled<T>(promises: Promise<T>[]): Promise<IResult<T>[]> {
  return Promise.all(
    promises.map((p) =>
      p
        .then((result: T) => {
          return {
            result: result,
            success: true,
            reason: null,
          };
        })
        .catch((reason) => {
          return {
            result: null,
            success: false,
            reason: reason,
          };
        }),
    ),
  );
}

export function PromiseAllIgnoreErrors<T>(promises: Promise<T>[]): Promise<(T | null)[]> {
  return Promise.all(promises.map((p) => p.catch((reason) => null)));
}

export function PromiseAllReturnErrors<T>(promises: Promise<T>[]): Promise<any[]> {
  return Promise.all(promises.map((p) => p.then((r) => null).catch((reason) => reason)));
}

export function delayedCall<T>(func: () => Promise<T>, waitMS: number): Promise<T> {
  return new Promise<T>((resolve) => {
    setTimeout(() => {
      resolve(func());
    }, waitMS);
  });
}

export function uuidToBuffer(uuid: string): Buffer {
  return Buffer.from(uuid.replace(/-/g, ""), "hex");
}
