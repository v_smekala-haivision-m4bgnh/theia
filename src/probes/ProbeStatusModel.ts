import "mongoose-paginate";
import { IDBDocument, IDBSchemaDefinition, MongoDBClient } from "@hai/lf-node-common";

export enum ProbeType {
  THUMBER = "thumber",
}

export interface ITopologyProbeStatus {
  createdAt: Date;
  topologyId: string;
  probeType: ProbeType;
  hubletIds: string[];
}

export interface ITopologyProbeStatusModel extends ITopologyProbeStatus, IDBDocument {}

const schemaDefinition: IDBSchemaDefinition = {
  createdAt: { type: Date, default: Date.now },
  topologyId: String,
  probeType: String,
  hubletIds: Array,
};
const TopologyProbeStatusSchema = MongoDBClient.createSchema<any>(schemaDefinition);
TopologyProbeStatusSchema.index({ topologyId: 1, probeType: 1 }, { unique: true });

export const TopologyProbeStatusModel = MongoDBClient.createModel<ITopologyProbeStatusModel>(
  "TopologyProbeStatus",
  TopologyProbeStatusSchema,
);
