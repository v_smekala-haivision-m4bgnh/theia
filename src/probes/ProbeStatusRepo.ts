import { injectable } from "inversify";
import "reflect-metadata";
import { MongoDBClient } from "@hai/lf-node-common";
import { ITopologyProbeStatus, ProbeType, TopologyProbeStatusModel } from "./ProbeStatusModel";

export interface IProbeStatusRepo {
  getStatusByProbeType(topologyId: string, probeType: ProbeType): Promise<ITopologyProbeStatus>;
  addHubletStatus(topologyId: string, probeType: ProbeType, hubletId: string): Promise<ITopologyProbeStatus>;
  removeHubletStatus(
    topologyId: string,
    probeType: ProbeType,
    hubletId: string,
  ): Promise<ITopologyProbeStatus | undefined>;
}

@injectable()
export class ProbeStatusRepo implements IProbeStatusRepo {
  public async getStatusByProbeType(topologyId: string, probeType: ProbeType): Promise<ITopologyProbeStatus> {
    const model = await TopologyProbeStatusModel.findOne({ topologyId, probeType });
    return model ? MongoDBClient.objClean(model.toObject()) : undefined;
  }

  public async addHubletStatus(
    topologyId: string,
    probeType: ProbeType,
    hubletId: string,
  ): Promise<ITopologyProbeStatus> {
    return await this.atomicUpsert<ITopologyProbeStatus>(async () => {
      const status = {
        topologyId: topologyId,
        probeType: probeType,
        $addToSet: { hubletIds: hubletId },
      };

      const model = await TopologyProbeStatusModel.findOneAndUpdate({ topologyId, probeType }, status, {
        new: true,
        upsert: true,
        setDefaultsOnInsert: true,
      });
      return MongoDBClient.objClean(model.toObject());
    });
  }

  public async removeHubletStatus(
    topologyId: string,
    probeType: ProbeType,
    hubletId: string,
  ): Promise<ITopologyProbeStatus | undefined> {
    return await this.atomicUpsert<ITopologyProbeStatus>(async () => {
      const status = {
        topologyId: topologyId,
        probeType: probeType,
        $pull: { hubletIds: hubletId },
      };

      const model = await TopologyProbeStatusModel.findOneAndUpdate({ topologyId, probeType }, status, { new: true });
      return model ? MongoDBClient.objClean(model.toObject()) : undefined;
    });
  }

  // Upserts operations are not atomic in Mongo and we may get duplicate key errors while trying to concurrently save a non-existing object.
  // A simple retry fixes the problem. See:
  // https://docs.mongodb.com/manual/reference/method/db.collection.findAndModify/#upsert-and-unique-index
  private async atomicUpsert<T>(func: () => Promise<T>, retry = false): Promise<T> {
    try {
      return await func();
    } catch (err) {
      if (!retry && err.codeName === "DuplicateKey") {
        return await this.atomicUpsert(func, true);
      } else {
        throw err;
      }
    }
  }
}
