import { KafkaTopic } from "@hai/lf-node-common";

export class Config {
  public static APP_NAME = "theia";
  public static HTTP_PORT = 17160;
  public static CORRELATION_ID: string = "cid";
  public static KAFKA_CONN = process.env.KAFKA_CONN;
  public static DB_CONN = "mongodb://mongodb:27017/esense";
  public static REDIS_CONN = "redis://redislru:6379";
  public static JUPITER_CONN = process.env.JUPITER_CONN || "http://jupiter";

  public static USER_TOKEN_EXPIRATION = 86400; // seconds: 1 day

  // Kafka
  public static TOPICS_PARTITIONS: number = parseInt(process.env.TOPICS_PARTITIONS || "3", 10);
  public static KAFKA_INSTANCES: number = parseInt(process.env.KAFKA_INSTANCES || "3", 10);

  public static THEIA_BROADCAST = "theia_broadcast";
  public static THEIA_TASKS = "theia_tasks";

  public static KAFKA_TOPICS_CREATE: KafkaTopic[] = [
    {
      name: Config.THEIA_TASKS,
      instanceConsumerGroup: false,
    },
    {
      name: Config.THEIA_BROADCAST,
      instanceConsumerGroup: true,
    },
  ];

  // Routes
  private static WS_PATH = "/live";
  public static THUMBS_PATH = "/thumb";
  public static THUMBS_WS_PATH = Config.WS_PATH + Config.THUMBS_PATH;
  public static USE_CERBERUS: boolean = process.env.USE_CERBERUS ? JSON.parse(process.env.USE_CERBERUS) : true;
}
