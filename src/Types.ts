import { COMMONTYPES } from "@hai/lf-node-common";

const TYPES = {
  IRoute: Symbol("IRoute"),
  IWSRoute: Symbol("IWSRoute"),
  IDBClient: COMMONTYPES.IDBClient,
  IKafkaClient: COMMONTYPES.IKafkaClient,
  IHealthRoute: COMMONTYPES.IHealthRoute,
  InstanceId: COMMONTYPES.InstanceId,
  IHttpServer: Symbol("IHttpServer"),
  ICacheClient: COMMONTYPES.ICacheClient,
  IAuthService: Symbol("IAuthService"),
  IAuthRepo: Symbol("IAuthRepo"),
  IThumbRoute: Symbol("IThumbRoute"),
  IThumbService: Symbol("IThumbService"),
  IProbeStatusRepo: Symbol("IProbeStatusRepo"),
  IWebSocketServer: Symbol("IWebSocketServer"),
};

export default TYPES;
