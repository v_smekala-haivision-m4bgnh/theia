import { inject, injectable } from "inversify";
import { IRouter, RequestHandler, Router } from "express";
import bodyParser from "body-parser";
import { createLogger, sendInternalServerErrorResponse, sendNotFoundErrorResponse } from "@hai/lf-node-common";
import { IRoute } from "../http/IRoute";
import TYPES from "../Types";
import { IThumbService } from "./ThumbService";
import { Config } from "../Config";
import { ITopologyProbeStatus } from "../probes/ProbeStatusModel";
import { IAuthService, TokenHandlerType } from "../auth/AuthService";

export interface IThumbRoute {
  create(): IRouter;
  destroy(): void;
}

@injectable()
export class ThumbRoute implements IRoute, IThumbRoute {
  private logger = createLogger("ThumbRoute");

  constructor(
    @inject(TYPES.IThumbService) private thumbService: IThumbService,
    @inject(TYPES.IAuthService) private authService: IAuthService,
  ) {}

  public path(): string {
    return Config.THUMBS_PATH;
  }

  public create(): IRouter {
    const router: Router = Router();

    router.get<any, ITopologyProbeStatus | IThumbRouteError>(
      "/topology/:topologyId/status",
      this.authService.authenticate(),
      this.authService.getTokenHandler({ accessType: TokenHandlerType.ALL }),
      async (req, res): Promise<void> => {
        const authHeader = req.get("Authorization") as string;
        const topologyId = req.params.topologyId;
        try {
          if (await this.thumbService.hasTopologyReadPermission(topologyId, req.user as Express.User, authHeader)) {
            const status = await this.thumbService.getStatus(topologyId);
            status ? res.status(200).send(status) : sendNotFoundErrorResponse(res);
          } else {
            res.status(403).send({
              message: "Not authorized to check thumbnail status for this topology",
              code: 403,
            });
            return;
          }
        } catch (err) {
          const message = `Error getting status for topologyId=${topologyId}`;
          this.logger.error(message, err);
          sendInternalServerErrorResponse(res, `${message}: ${err.message}`);
        }
      },
    );

    router.get<any, void | IThumbRouteError>(
      "/topology/:topologyId/hublet/:hubletId/start",
      this.authService.authenticate(),
      async (req, res): Promise<void> => {
        const authHeader = req.get("Authorization") as string;
        try {
          if (
            await this.thumbService.hasTopologyPermission(
              req.params.topologyId,
              "hub:HubletsThumbnailStart",
              req.user as Express.User,
              authHeader,
            )
          ) {
            await this.thumbService.startProbe(req.params.topologyId, req.params.hubletId, authHeader);
            res.status(200).send();
          } else {
            res.status(403).send({
              message: "Not authorized to start thumbnail for this topology",
              code: 403,
            });
            return;
          }
        } catch (err) {
          const message = `Error starting thumber probe for topologyId=${req.params.topologyId}, hubletId=${req.params.hubletId}`;
          this.logger.error(message, err);
          sendInternalServerErrorResponse(res, `${message}: ${err.message}`);
        }
      },
    );

    router.get<any, void | IThumbRouteError>(
      "/topology/:topologyId/hublet/:hubletId/stop",
      this.authService.authenticate(),
      async (req, res): Promise<void> => {
        const authHeader = req.get("Authorization") as string;
        try {
          if (
            await this.thumbService.hasTopologyPermission(
              req.params.topologyId,
              "hub:HubletsThumbnailStop",
              req.user as Express.User,
              authHeader,
            )
          ) {
            await this.thumbService.stopProbe(req.params.topologyId, req.params.hubletId, authHeader);
            res.status(200).send();
          } else {
            res.status(403).send({
              message: "Not authorized to stop thumbnail for this topology",
              code: 403,
            });
            return;
          }
        } catch (err) {
          const message = `Error stopping thumber probe for topologyId=${req.params.topologyId}, hubletId=${req.params.hubletId}`;
          this.logger.error(message, err);
          sendInternalServerErrorResponse(res, `${message}: ${err.message}`);
        }
      },
    );

    router.get<any, Buffer | IThumbRouteError>(
      "/topology/:topologyId/hublet/:hubletId/thumb",
      this.authService.authenticate(),
      async (req, res): Promise<void> => {
        try {
          const authHeader = req.get("Authorization") as string;
          if (
            await this.thumbService.hasTopologyReadPermission(
              req.params.topologyId,
              req.user as Express.User,
              authHeader,
            )
          ) {
            // Topology Id is not really needed to retrieve the thumb, this is just for API consistency
            const thumb = await this.thumbService.getThumb(req.params.hubletId);
            thumb ? res.status(200).contentType("image/jpeg").send(thumb) : sendNotFoundErrorResponse(res);
          } else {
            res.status(403).send({
              message: "Not authorized to get thumbnail for this topology",
              code: 403,
            });
            return;
          }
        } catch (err) {
          const message = `Error getting thumbnail for topologyId=${req.params.topologyId}, hubletId=${req.params.hubletId}`;
          this.logger.error(message, err);
          sendInternalServerErrorResponse(res, `${message}: ${err.message}`);
        }
      },
    );

    router.post(
      "/probe/topology/:topologyId/hublet/:hubletId",
      this.getFFmpegRawHandler(),
      async (req, res): Promise<void> => {
        // TODO: see how to protect this endpoint while remaining compatible with ffmpeg
        // Does not wait to prevent ffmpeg from waiting
        this.thumbService
          .setThumb(req.params.topologyId, req.params.hubletId, req.body)
          .catch((err) =>
            this.logger.error(
              `Error setting thumb for topologyId=${req.params.topologyId}, hubletId=${req.params.hubletId}`,
              err,
            ),
          );
        res.status(200).send({});
      },
    );

    return router;
  }

  public destroy(): void {
    // nothing to do here
  }

  public getRequestHandlers(): RequestHandler[] {
    return [];
  }

  private getFFmpegRawHandler(): RequestHandler {
    // FFmpeg does not set the content-type for some obscure reason, so I am using this 'icy-metadata' header to enable the raw body parser
    return bodyParser.raw({ type: (req) => req.headers["icy-metadata"], limit: "1000kb" });
  }
}

interface IThumbRouteError {
  message: string;
  code: number;
}
