export interface IThumbMessage {
  type: "PROBE_START" | "PROBE_STOP" | "NEW_THUMB";
  payload: IThumbMessagePayload;
}

export interface IThumbMessagePayload {
  topologyId: string;
  hubletId: string;
}

export type ThumbCallback = (event: IThumbMessage) => void;

export interface INewThumbPayload extends IThumbMessagePayload {
  thumb: Buffer;
}
