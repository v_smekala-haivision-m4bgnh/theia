import NodeCache from "node-cache";
import { inject, injectable } from "inversify";
import "reflect-metadata";
import got from "got";
import http from "http";
import TYPES from "../Types";
import { createLogger, ICacheClient, IKafkaClient } from "@hai/lf-node-common";
import { Config } from "../Config";
import { ITheiaBroadcastMessage } from "../common/Common";
import { delayedCall } from "../Utils";
import { INewThumbPayload, ThumbCallback, IThumbMessage } from "./ThumbModel";
import { IProbeStatusRepo } from "../probes/ProbeStatusRepo";
import { ITopologyProbeStatus, ProbeType } from "../probes/ProbeStatusModel";
import { IAuthService } from "../auth/AuthService";
import { ITokenEntity } from "@hai/sancus-lib";

export interface IThumbService {
  getStatus(topologyId: string): Promise<ITopologyProbeStatus | undefined>;
  startProbe(topologyId: string, hubletId: string, authHeader: string): Promise<void>;
  stopProbe(topologyId: string, hubletId: string, authHeader: string): Promise<void>;
  getThumb(hubletId: string): Promise<Buffer | undefined>;
  setThumb(topologyId: string, hubletId: string, data: Buffer): Promise<void>;
  registerListener(topologyId: string, listener: ThumbCallback): void;
  unregisterListener(topologyId: string, listener: ThumbCallback): void;
  getTopologyData(topologyId: string, authHeader: string): Promise<ITopologyData | undefined>;
  hasTopologyReadPermission(topologyId: string, user: Express.User, authHeader: string): Promise<boolean>;
  hasTopologyPermission(
    topologyId: string,
    permission: string,
    user: Express.User,
    authHeader: string,
  ): Promise<boolean>;
}

interface ITopologyData {
  organizationId?: string;
  subscriptionId?: string;
  routeId?: string;
}

interface ILiveWorkflowRequest {
  labels?: { [key: string]: string };
}

interface ILiveWorkflow {
  liveWorkflowRequest: ILiveWorkflowRequest;
}

interface ILiveWorkflowStatus {
  liveWorkflow: ILiveWorkflow;
  // workflowsStatus: IWorkflowStatus[];
}

@injectable()
export class ThumbService implements IThumbService {
  private readonly agent: http.Agent;
  private readonly CACHE_PREFIX = "thumbnail";
  private readonly TOPOLOGY_DATA_CACHE_PREFIX = "topology";
  private readonly THUMB_EXPIRE_SEC = 20; // Thumbs expire in 20 seconds
  private readonly LOCAL_CACHE_TTL = 604800; // One week

  private logger = createLogger("ThumbService");
  private listenersByTopologyId = new NodeCache({ stdTTL: this.LOCAL_CACHE_TTL, useClones: false }); // TTL is a week

  constructor(
    @inject(TYPES.IProbeStatusRepo) private probeStatusRepo: IProbeStatusRepo,
    @inject(TYPES.IKafkaClient) private kafkaClient: IKafkaClient,
    @inject(TYPES.ICacheClient) private cacheClient: ICacheClient,
    @inject(TYPES.IAuthService) private authService: IAuthService,
  ) {
    this.agent = new http.Agent();
    this.kafkaClient.registerTopicListener<ITheiaBroadcastMessage>(Config.THEIA_BROADCAST, (key, message) =>
      this.onBroadcastMessage(key, message),
    );
  }

  private async onBroadcastMessage(_: string | undefined, message: ITheiaBroadcastMessage) {
    if (message.type === "THUMB_MESSAGE") {
      // this.logger.debug(`Thumb message type=${message.payload.type} topologyId=${message.payload.topologyId} hubletId=${message.payload.hubletId}`);
      this.notifyThumbMessage(message.payload);
    }
  }

  public getStatus(topologyId: string): Promise<ITopologyProbeStatus | undefined> {
    return this.probeStatusRepo.getStatusByProbeType(topologyId, ProbeType.THUMBER);
  }

  public async startProbe(topologyId: string, hubletId: string, authHeader: string): Promise<void> {
    try {
      // Calls jupiter to create the probe
      const url = `${Config.JUPITER_CONN}/live-workflow/${topologyId}/hublet/${hubletId}/probe/${ProbeType.THUMBER}`;
      await got.post(url, {
        agent: this.agent,
        headers: { Authorization: authHeader, "Content-Type": "application/json" },
      });
    } catch (err) {
      if (err?.response?.statusCode === 412) {
        // 412 means a thumber probe is already present, ignore it and return
        this.logger.warn(
          `Thumber probe is already present (412) for topologyId=${topologyId}, hubletId=${hubletId}. Ignoring...`,
        );
        return;
      } else {
        throw err;
      }
    }

    await this.probeStatusRepo.addHubletStatus(topologyId, ProbeType.THUMBER, hubletId);
    this.sendThumbMessage({
      type: "PROBE_START",
      payload: {
        topologyId: topologyId,
        hubletId: hubletId,
      },
    });
  }

  public async hasTopologyPermission(
    topologyId: string,
    permission: string,
    user: Express.User,
    authHeader: string,
  ): Promise<boolean> {
    if (user && user.rs) {
      const topologyData = await this.getTopologyData(topologyId, authHeader);
      if (topologyData?.organizationId && topologyData.subscriptionId) {
        return this.authService.hasMembershipPermission(
          topologyData?.organizationId,
          topologyData?.subscriptionId,
          permission,
          user as ITokenEntity,
        );
      } else {
        return false;
      }
    } else {
      // Cerberus backward compatibility to be removed
      return Config.USE_CERBERUS && user && (user as any).role;
    }
  }

  public hasTopologyReadPermission(topologyId: string, user: Express.User, authHeader: string): Promise<boolean> {
    return this.hasTopologyPermission(topologyId, "hub:HubletsRoutesGet", user, authHeader);
  }

  public async getTopologyData(topologyId: string, authHeader: string): Promise<ITopologyData | undefined> {
    const topologyDataFromCache = await this.getTopologyDataFromCache(topologyId);
    if (topologyDataFromCache) {
      return topologyDataFromCache;
    }
    this.logger.debug(`Calling Jupiter to obtain topologyData for ${topologyId}`);
    const url = `${Config.JUPITER_CONN}/live-workflow/${topologyId}?removed=true`;
    try {
      const resp = await got.get<string>(url, {
        agent: this.agent,
        headers: { Authorization: authHeader, "Content-Type": "application/json" },
      });
      const liveWorkflowStatus = JSON.parse(resp.body) as ILiveWorkflowStatus;
      const subscriptionId =
        liveWorkflowStatus.liveWorkflow.liveWorkflowRequest.labels?.["srthub.havision.com/subscriptionId"];
      const organizationId =
        liveWorkflowStatus.liveWorkflow.liveWorkflowRequest.labels?.["srthub.havision.com/organizationId"];
      const routeId = liveWorkflowStatus.liveWorkflow.liveWorkflowRequest.labels?.["srthub.havision.com/routeId"];
      const topologyData = {
        organizationId,
        subscriptionId,
        routeId,
      };
      this.saveTopologyData(topologyId, topologyData);
      return topologyData;
    } catch (err) {
      if (err.response?.statusCode === 404) {
        return undefined;
      }
      this.logger.error("Error getting topology data", err);
      throw err;
    }
  }

  public async stopProbe(topologyId: string, hubletId: string, authHeader: string): Promise<void> {
    try {
      // Calls jupiter to delete the probe
      const url = `${Config.JUPITER_CONN}/live-workflow/${topologyId}/hublet/${hubletId}/probe/${ProbeType.THUMBER}`;
      await got.delete(url, {
        agent: this.agent,
        headers: { Authorization: authHeader, "Content-Type": "application/json" },
      });
    } catch (err) {
      if (err?.response?.statusCode === 404) {
        // 404, probe not found: ignore it and return
        this.logger.warn(
          `Thumber stop probe returned 404 for topologyId=${topologyId}, hubletId=${hubletId}. Ignoring...`,
        );
        await this.probeStatusRepo.removeHubletStatus(topologyId, ProbeType.THUMBER, hubletId);
        return;
      } else {
        throw err;
      }
    }

    await this.probeStatusRepo.removeHubletStatus(topologyId, ProbeType.THUMBER, hubletId);
    this.sendThumbMessage({
      type: "PROBE_STOP",
      payload: {
        topologyId: topologyId,
        hubletId: hubletId,
      },
    });
  }

  public async getThumb(hubletId: string): Promise<Buffer | undefined> {
    const buffStr = await this.cacheClient.get(this.CACHE_PREFIX, hubletId);
    return buffStr ? Buffer.from(buffStr, "binary") : undefined;
  }

  public async setThumb(topologyId: string, hubletId: string, data: Buffer): Promise<void> {
    // Saves the thumb in cache and notifies it
    // this.logger.debug(`Saving thumb topologyId=${topologyId} hubletId=${hubletId}`);
    await this.saveThumb(hubletId, data);
    this.sendThumbMessage({
      type: "NEW_THUMB",
      payload: {
        topologyId: topologyId,
        hubletId: hubletId,
      },
    });
  }

  public registerListener(topologyId: string, listener: ThumbCallback): void {
    let listeners = this.listenersByTopologyId.get<ThumbCallback[]>(topologyId);
    if (!listeners) {
      listeners = [];
    }
    listeners.push(listener);
    this.listenersByTopologyId.set(topologyId, listeners, this.LOCAL_CACHE_TTL);
  }

  public unregisterListener(topologyId: string, listener: ThumbCallback): void {
    let listeners = this.listenersByTopologyId.get<ThumbCallback[]>(topologyId);
    if (listeners) {
      listeners = listeners.filter((l) => l !== listener);
      this.listenersByTopologyId.set(topologyId, listeners, this.LOCAL_CACHE_TTL);
    }
  }

  private async notifyThumbMessage(message: IThumbMessage): Promise<void> {
    const listeners = this.listenersByTopologyId.get<ThumbCallback[]>(message.payload.topologyId);
    if (listeners && listeners.length > 0) {
      if (message.type === "NEW_THUMB") {
        // New thumb is available
        const thumb = await this.getThumb(message.payload.hubletId);
        if (thumb) {
          (message.payload as INewThumbPayload).thumb = thumb;
          listeners.forEach((listener) => listener(message));
        }
      } else if (message.type === "PROBE_START" || message.type === "PROBE_STOP") {
        // Probe start/stop event
        listeners.forEach((listener) => listener(message));
      }
    }
  }

  private async saveThumb(hubletId: string, data: Buffer, retry = 0): Promise<void> {
    try {
      await this.cacheClient.set(this.CACHE_PREFIX, hubletId, data.toString("binary"), this.THUMB_EXPIRE_SEC);
    } catch (err) {
      if (retry === 5) {
        throw err;
      } else {
        return delayedCall<void>(() => this.saveThumb(hubletId, data, ++retry), 500);
      }
    }
  }

  private async saveTopologyData(topologyId: string, topologyData: ITopologyData, retry = 0): Promise<void> {
    try {
      await this.cacheClient.set(
        this.TOPOLOGY_DATA_CACHE_PREFIX,
        topologyId,
        JSON.stringify(topologyData),
        this.LOCAL_CACHE_TTL,
      );
    } catch (err) {
      if (retry === 5) {
        throw err;
      } else {
        return delayedCall<void>(() => this.saveTopologyData(topologyId, topologyData, ++retry), 500);
      }
    }
  }

  private async getTopologyDataFromCache(topologyId: string): Promise<ITopologyData | undefined> {
    const topologyDataS = await this.cacheClient.get(this.TOPOLOGY_DATA_CACHE_PREFIX, topologyId);
    return topologyDataS ? JSON.parse(topologyDataS) : undefined;
  }

  private sendThumbMessage(message: IThumbMessage): void {
    const theiaMessage: ITheiaBroadcastMessage = {
      type: "THUMB_MESSAGE",
      payload: message,
    };
    this.kafkaClient.sendMessage(Config.THEIA_BROADCAST, theiaMessage);
  }
}
