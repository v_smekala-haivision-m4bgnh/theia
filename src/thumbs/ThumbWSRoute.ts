import { inject, injectable } from "inversify";
import { Express, Request } from "express";
import expressWs, { WebsocketRequestHandler } from "express-ws";
import * as WebSocket from "ws";
import TYPES from "../Types";
import correlationId from "@hai/lf-node-common/common/CorrelationId";
import { Config } from "../Config";
import { IUser, createLogger, WSMessage } from "@hai/lf-node-common";
import { IWSRoute } from "../http/IRoute";
import { IThumbService } from "./ThumbService";
import { IAuthService } from "../auth/AuthService";
import { WebSocketRoute } from "../http/WebSocketRoute";
import { uuidToBuffer } from "../Utils";
import { INewThumbPayload, IThumbMessage, ThumbCallback } from "./ThumbModel";
import { ExtractJwt } from "passport-jwt";

export interface IThumbWSRoute {
  create(app: Express): void;
  destroy(): void;
}

@injectable()
export class ThumbWSRoute extends WebSocketRoute implements IWSRoute, IThumbWSRoute {
  private appWs: expressWs.Instance | undefined;

  constructor(
    @inject(TYPES.IAuthService) private authService: IAuthService,
    @inject(TYPES.IThumbService) private thumbService: IThumbService,
  ) {
    super();
    this.logger = createLogger("ThumbWSRoute");
  }

  public path(): string {
    return `${Config.THUMBS_WS_PATH}/topology/:id`;
  }

  public listenersCount(): number {
    return this.appWs?.getWss().clients.size ?? 0;
  }

  public create(app: Express): void {
    this.appWs = expressWs(app);
    this.appWs.app.ws(this.path(), this.passportAuthenticate(), this.thumbnailWS());
  }

  public destroy(): void {
    /**/
  }

  private thumbnailWS(): WebsocketRequestHandler {
    return (async (ws: WebSocket, req: Request) => {
      const user = req.user as Express.User;
      const topologyId: string = req.params.id;
      const tokenParam = ExtractJwt.fromUrlQueryParameter("token")(req);
      this.logger.debug(`WS thumb subscription for topology=${topologyId}`);

      if (!user) {
        this.sendAuthError(ws, new Error("No user"));
        ws.close();
        return;
      } else {
        const authHeader = "Bearer " + tokenParam;
        const permission = await this.thumbService.hasTopologyReadPermission(topologyId, user, authHeader);
        if (!permission) {
          this.sendAuthError(ws, new Error("User does not have permission to read this topology"));
          ws.close();
          return;
        } else {
          this.logger.debug(`User has read permission for this topology`);
        }
      }

      const onThumbEvent: ThumbCallback = (event: IThumbMessage) => {
        let message: Buffer | WSMessage;
        if (event.type === "NEW_THUMB") {
          // Message will consist on a buffer composed of the hubletId and the thumb
          message = Buffer.concat([uuidToBuffer(event.payload.hubletId), (event.payload as INewThumbPayload).thumb]);
        } else {
          // Message is a WSMessage
          message = {
            type: event.type,
            data: event.payload,
            target: {} as any,
          };
        }

        this.logger.debug(
          `WS sending thumb event type=${event.type}, topology=${event.payload.topologyId}, hubletId=${event.payload.hubletId}`,
        );
        ws.send(Buffer.isBuffer(message) ? message : JSON.stringify(message), (err: Error | null) => {
          if (err) {
            this.logger.debug(
              `WS send thumbnail error, topology=${event.payload.topologyId}, hubletId=${event.payload.hubletId}`,
              err,
            );
          }
        });
      };

      if (this.authService.validateUser(user)) {
        this.thumbService.registerListener(topologyId, onThumbEvent);
        ws.on("close", () => {
          // this.logger.debug(`Closing websocket topology=${topologyId}`)
          this.thumbService.unregisterListener(topologyId, onThumbEvent);
        });
      }
    }) as any;
  }
}
