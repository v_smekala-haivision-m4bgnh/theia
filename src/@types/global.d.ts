import sancusLib from "@hai/sancus-lib";

declare global {
  namespace Express {
    interface User extends sancusLib.ITokenEntity {
      role?: UserRole;
    }
  }
}
