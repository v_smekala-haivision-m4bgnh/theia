import { injectable } from "inversify";
import "reflect-metadata";
import { NextFunction, Request, Response } from "express";
import { Config } from "../Config";
import {
  createLogger,
  IAccount,
  IUser,
  UserRole,
  sendErrorResponse,
  sendForbiddenErrorResponse,
  sendUnauthorizedErrorResponse,
} from "@hai/lf-node-common";
import passport from "passport";
import {
  haiEntityHasPermissionMembership,
  haiEntityHasPermission,
  haiEntityHasAnyPermissionPrefix,
  ITokenEntity,
} from "@hai/sancus-lib";

export class AuthMessage {
  constructor(public type: AuthMessageType, public username: string, public data?: any) {}
}

export enum AuthMessageType {
  SET_BLACKLIST_USERNAME = "SET_BLACKLIST_USERNAME",
}

export class TokenHandlerOptions {
  public accessType!: TokenHandlerType;
}

export enum TokenHandlerType {
  API_ONLY = "API_ONLY",
  USER_ONLY = "USER_ONLY",
  ALL = "ALL",
  ALL_GUEST = "ALL_GUEST",
}

export enum Permission {
  COGNITIVE_SERVICES = "cognitive-services",
  LIVE_STREAMS = "live-streams",
  PERCEPTUAL_QUALITY_PER_TITLE_4k = "perceptual-quality-per-title-4k",
  DEVICE_AWARE_OPTIMIZATION = "device-aware-optimization",
  NETWORK_AWARE_OPTIMIZATION = "network-aware-optimization",
  ULTRA_FAST_ENCODING = "ultra-fast-encoding",
  YOUTUBE_PUBLISHING = "youtube-publishing",
  WATCH_FOLDER = "watch-folder",
  VOD_WORKFLOWS = "vod_workflows",
}

export interface IAuthService {
  authenticate(): (req: Request, res: Response, next: NextFunction) => void;

  validateUser(user: Express.User): boolean;

  getTokenHandler(options: TokenHandlerOptions): (req: Request, res: Response, next: NextFunction) => void;

  getPreflightHandler(): (req: Request, res: Response, next: NextFunction) => void;

  checkPermission(user: IUser, account: IAccount, permission: Permission): boolean;

  hasMembershipPermission(
    organizationId: string,
    subscriptionId: string,
    theiaRead: string,
    user: ITokenEntity,
  ): boolean;
}

@injectable()
export class AuthService implements IAuthService {
  private logger = createLogger("AuthService");

  public authenticate(): (req: Request, res: Response, next: NextFunction) => void {
    return (req, res, next) => {
      if (Config.USE_CERBERUS) {
        try {
          passport.authenticate("cerberus", { session: false }, (err, user, info) => {
            if (err) {
              this.logger.error("Error authenticating Cerberus " + err);
              this.authenticateHaidentity(req, res, next);
            } else {
              if (!user) {
                this.authenticateHaidentity(req, res, next);
              } else {
                req.user = user;
                next();
              }
            }
          })(req, res, next);
        } catch (e) {
          this.logger.error("Error authenticating Cerberus " + e);
        }
      } else {
        this.authenticateHaidentity(req, res, next);
      }
    };
  }

  private authenticateHaidentity(req: Request, res: Response, next: NextFunction) {
    try {
      passport.authenticate("haidentity", { session: false }, (err, user) => {
        if (err) {
          this.logger.error("Error authenticating Haidentity " + err);
          sendUnauthorizedErrorResponse(res, `${err}`);
        } else if (!user) {
          sendUnauthorizedErrorResponse(res, `Empty Haidentity user`);
        } else {
          req.user = user;
          next();
        }
      })(req, res, next);
    } catch (e) {
      this.logger.error("Error authenticating Haidentity " + e);
      sendUnauthorizedErrorResponse(res, `${e}`);
    }
  }

  public validateUser(user: Express.User): boolean {
    return (
      (user.role &&
        (user.role === UserRole.USER || user.role === UserRole.USER_READONLY || user.role === UserRole.ADMIN)) ||
      (user.rs && user.ms)
    );
  }

  public validateGuest(user: Express.User): boolean {
    return user.role === UserRole.GUEST;
  }

  public getTokenHandler(options: TokenHandlerOptions): (req: Request, res: Response, next: NextFunction) => void {
    const getAuthBearerToken = (req: Request): string => {
      try {
        return (req.get("authorization") as string).split(" ")[1];
      } catch (err) {
        throw new Error("Error parsing Auth Bearer token: " + err.message);
      }
    };

    const handleAllGuestToken = (req: Request, res: Response, next: NextFunction) => {
      if (this.validateGuest(req.user as Express.User) || this.validateUser(req.user as Express.User)) {
        next();
      } else {
        sendUnauthorizedErrorResponse(res);
      }
    };

    const handleAllToken = (req: Request, res: Response, next: NextFunction) => {
      if (this.validateUser(req.user as Express.User)) {
        next();
      } else {
        sendUnauthorizedErrorResponse(res);
      }
    };

    const handleUserToken = (req: Request, res: Response, next: NextFunction) => {
      if (this.validateUser(req.user as Express.User)) {
        next();
      } else {
        sendUnauthorizedErrorResponse(res);
      }
    };

    return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      try {
        if (req.user) {
          if (options.accessType === TokenHandlerType.ALL_GUEST) {
            handleAllGuestToken(req, res, next);
          } else if (options.accessType === TokenHandlerType.ALL) {
            handleAllToken(req, res, next);
          } else if (options.accessType === TokenHandlerType.USER_ONLY) {
            handleUserToken(req, res, next);
          } else {
            sendUnauthorizedErrorResponse(res);
          }
        } else {
          sendForbiddenErrorResponse(res);
        }
      } catch (err) {
        this.logger.error(err);
        sendErrorResponse(res, err);
      }
    };
  }

  public getPreflightHandler(): (req: Request, res: Response, next: NextFunction) => void {
    return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
      if (req.method === "OPTIONS") {
        res.status(200).send();
      } else {
        next();
      }
    };
  }

  public checkPermission(user: IUser, account: IAccount, permission: Permission): boolean {
    if (user.role === UserRole.ADMIN) {
      return true;
    } else {
      if (account.permissions) {
        const userPermission = account.permissions.find((perm) => perm.id === permission);
        if (userPermission) {
          return userPermission.granted;
        }
      }
      return false;
    }
  }

  public hasMembershipPermission(
    organizationId: string,
    subscriptionId: string,
    permission: string,
    user: ITokenEntity,
  ): boolean {
    this.logger.info(`CHECKING PERMISSION ${permission} FOR ${organizationId} ${subscriptionId}`);
    const subPermission = haiEntityHasPermissionMembership(user, permission, subscriptionId, true);
    return subPermission || haiEntityHasPermissionMembership(user, permission, organizationId, true);
  }

  private isHaiUser(expressUser: Express.User) {
    return expressUser.rs;
  }

  private haiUserHasTheiaPermission(tokenEntity: ITokenEntity): boolean {
    return haiEntityHasAnyPermissionPrefix(tokenEntity, "theia");
  }

  private haiUserHasPermission(tokenEntity: ITokenEntity, permission: string): boolean {
    return haiEntityHasPermission(tokenEntity, permission) !== undefined;
  }
}
