import { Express } from "express";
import { IRouter, RequestHandler } from "express";

export interface IRoute {
  path(): string;
  create(): IRouter;
  getRequestHandlers(): RequestHandler[];
  destroy(): void;
}

export interface IWSRoute {
  path(): string;
  listenersCount(): number;
  create(app: Express): void;
  destroy(): void;
}
