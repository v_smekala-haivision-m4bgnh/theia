import { injectable } from "inversify";
import jwt from "jsonwebtoken";
import WebSocket from "ws";
import { Express, Request, NextFunction } from "express";
import { Config as CommonConfig, WSMessage, WSMessageType, createLogger } from "@hai/lf-node-common";
import { WebsocketRequestHandler } from "express-ws";
import { ExtractJwt } from "passport-jwt";
import { HaidentityAuthUtils } from "@hai/sancus-lib";

export interface IWebSocketServer {
  init(app: Express): void;
  stop(): void;
}

@injectable()
export abstract class WebSocketRoute {
  protected logger = createLogger("WebSocketRoute");

  protected passportAuthenticate(): WebsocketRequestHandler {
    return (async (ws: WebSocket, req: Request, next: NextFunction): Promise<void> => {
      const tokenHeader = ExtractJwt.fromAuthHeaderAsBearerToken()(req);
      const tokenParam = ExtractJwt.fromUrlQueryParameter("token")(req);
      const token = tokenHeader || tokenParam || "";
      try {
        req.user = jwt.verify(token, CommonConfig.JWT_SECRET) as Express.User | undefined;
        next();
      } catch (error) {
        this.logger.debug(`Cerberus authentication failed, trying Haidentity`);
        try {
          req.user = await HaidentityAuthUtils.haidentityKeyVerify(token);
          next();
        } catch (error2) {
          this.logger.debug(`Haidentity authentication failed`, error2.message);
          this.sendAuthError(ws, error2);
          ws.close();
        }
      }
    }) as any;
  }

  protected sendAuthError(ws: WebSocket, error: Error) {
    this.logger.debug("WS sending auth error: " + error?.message);
    ws.send(JSON.stringify(new WSMessage(WSMessageType.AUTH_ERROR)), (err: Error | null) => {
      if (err) {
        this.logger.debug("WS send error", err);
      }
    });
  }
}
