import { Express } from "express";
import { injectable, multiInject } from "inversify";
import TYPES from "../Types";
import { IWSRoute } from "./IRoute";
import { createLogger } from "@hai/lf-node-common";

export interface IWebSocketServer {
  init(app: Express): void;
  listenersCount(): number;
  stop(): void;
}

@injectable()
export class WebSocketServer implements IWebSocketServer {
  private logger = createLogger("WebSocketServer");

  constructor(@multiInject(TYPES.IWSRoute) private registrableWSRoutes: IWSRoute[]) {}

  public init(app: Express) {
    // Registrable websocket routes
    this.registrableWSRoutes.forEach((route) => {
      this.logger.info(`Registering route ${route.path()}`);
      route.create(app);
    });
  }

  public listenersCount(): number {
    let count = 0;
    this.registrableWSRoutes.forEach((route) => (count += route.listenersCount()));
    return count;
  }

  public stop(): void {
    this.registrableWSRoutes.forEach((route) => route.destroy());
  }
}
