import "reflect-metadata";
import { inject, injectable, multiInject } from "inversify";
import morgan from "morgan";
import express, { Express, Request, Response } from "express";
import helmet from "helmet";
import { Server } from "http";
import passport from "passport";
import passportJwt from "passport-jwt";
import { IRoute } from "./IRoute";
import { Config } from "../Config";
import { IWebSocketServer } from "./WebSocketServer";
import { Config as CommonConfig, sendBadRequestErrorResponse, createLogger, IHealthRoute } from "@hai/lf-node-common";
import correlationId from "@hai/lf-node-common/common/CorrelationId";
import TYPES from "../Types";
import { HaidentityAuthUtils } from "@hai/sancus-lib";

export interface IHttpServer {
  init(): Promise<Express>;
  stop(): Promise<void>;
}

@injectable()
export class HttpServer implements IHttpServer {
  private app: Express;
  private server!: Server;
  private routes: IRoute[] = [];
  private port: number = Config.HTTP_PORT;
  private logger = createLogger("HttpServer");

  public constructor(
    @inject(TYPES.IHealthRoute) private healthRoute: IHealthRoute,
    @inject(TYPES.IWebSocketServer) private webSocketServer: IWebSocketServer,
    @multiInject(TYPES.IRoute) private registrableRoutes: IRoute[],
  ) {
    this.app = express();
  }

  public init(): Promise<Express> {
    return new Promise<Express>(async (resolve, reject) => {
      this.app.use(express.json());
      this.app.use((error: any, req: Request, res: Response, next: () => void) => {
        if (error instanceof SyntaxError) {
          sendBadRequestErrorResponse(res, "Invalid JSON");
        } else {
          next();
        }
      });

      this.app.use(
        morgan("combined", {
          stream: { write: (message) => this.logger.debug(message.trim()) },
        }),
      );

      this.app.use(helmet());

      if (process.env.NODE_ENV === "dev") {
        this.setDevOptions();
      }

      this.app.get(["/", "/health"], (_req: express.Request, res: express.Response): void => {
        const health = this.healthRoute.getHealth();

        res.json({
          ...health,
          service: {
            wsListenersCount: this.webSocketServer.listenersCount(),
          },
        });
      });

      // Registrable routes
      this.registrableRoutes.forEach((route) => this.registerRoute(route));

      // JWT auth
      this.app.use(passport.initialize());
      passport.use("cerberus", HttpServer.getJWTStrategy());
      passport.use("haidentity", HaidentityAuthUtils.passwordJwksStrategy());

      // Correlation ID
      this.app.use(correlationId.middleware());

      // Init websocket server
      this.webSocketServer.init(this.app);

      this.server = this.app
        .listen(this.port, () => {
          this.logger.info(`Express listening on port ${this.port}`);
          resolve(this.app);
        })
        .on("error", (err) => {
          reject(err);
        });
    });
  }

  private registerRoute(route: IRoute): void {
    this.logger.info(`Registering route ${route.path()}`);
    this.app.use(route.path(), route.getRequestHandlers(), route.create());
    this.routes.push(route);
  }

  public stop(): Promise<void> {
    this.logger.warn(`Stopping server`);
    this.routes.forEach((route) => route.destroy());
    this.webSocketServer.stop();

    return new Promise<void>(async (resolve, _) => {
      if (this.server) {
        this.server.close(() => {
          resolve();
        });
      } else {
        resolve();
      }
    });
  }

  private static getJWTStrategy(): passportJwt.Strategy {
    return new passportJwt.Strategy(
      {
        jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: CommonConfig.JWT_SECRET,
      },
      (jwtPayload, cb) => {
        return cb(null, jwtPayload);
      },
    );
  }

  private setDevOptions(): void {
    // this.port =  Utils.getRandomPort();
    this.app.options("/*", (req, res, next) => {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "*, authorization");
      res.header("Access-Control-Allow-Methods", "*");
      next();
    });

    this.app.get("/*", (req, res, next) => {
      res.header("Access-Control-Allow-Origin", "*");
      next();
    });

    this.app.delete("/*", (req, res, next) => {
      res.header("Access-Control-Allow-Origin", "*");
      next();
    });

    this.app.post("/*", (req, res, next) => {
      res.header("Access-Control-Allow-Origin", "*");
      next();
    });

    this.app.put("/*", (req, res, next) => {
      res.header("Access-Control-Allow-Origin", "*");
      next();
    });
  }
}
