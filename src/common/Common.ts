import { IThumbMessage } from "../thumbs/ThumbModel";

export interface ITheiaBroadcastMessage {
  type: "THUMB_MESSAGE";
  payload: IThumbMessage;
}

export interface ITheiaTaskMessage {
  type: "";
  payload: any;
}
